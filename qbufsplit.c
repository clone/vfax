#include <assert.h>

#include "qbufsplit.h"

void qbuf_float_split(struct qbuf* sq, struct qbuf* dq1, struct qbuf*dq2) {
    float *sp, *dp1, *dp2;
    size_t sl, dl1, dl2;
    size_t c = 0;
    
    assert(sq && dq1 && dq2);

    sp = qbuf_pull(sq, &sl);
    dp1 = qbuf_push(dq1, &dl1);
    dp2 = qbuf_push(dq2, &dl2);

    assert(sp && dp1 && dp2);

    while (sl >= 2*sizeof(float) && dl1 >= sizeof(float) && dl2 >= sizeof(float)) {

        *(dp1++) = *(sp++);
        *(dp2++) = *(sp++);

        sl -= 2*sizeof(float);
        dl1 -= sizeof(float);
        dl2 -= sizeof(float);

        c += sizeof(float);
    }
    
    qbuf_pull_invalidate(sq, c*2);
    qbuf_push_validate(dq1, c);
    qbuf_push_validate(dq2, c);
}

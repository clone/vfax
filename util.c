#include <unistd.h>

#include "util.h"

ssize_t loop_write(int fd, const void *b, size_t l) {
    ssize_t t = 0;
    while (l) {
        ssize_t r;

        if ((r = write(fd, b, l)) < 0)
            return t == 0 ? r : t;
        else if (r == 0)
            return t;

        b += r;
        l -= r;
        t += r;
    }

    return t;
}

#ifndef foov17modhfoo
#define foov17modhfoo

#include <inttypes.h>

struct v17mod_state {
    int sample;
    int symbol_index;

    int seconds;
    
    float *xsymbols, *ysymbols;
    int i_symbols, n_symbols;
};

void v17mod_init(struct v17mod_state *s);
void v17mod_done(struct v17mod_state *s);

void v17mod_push(struct v17mod_state *s, const uint8_t *p, int l);
int v17mod_pull(struct v17mod_state *s, float *p, int l);

#endif

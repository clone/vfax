#ifndef fooqbufsplithfoo
#define fooqbufsplithfoo

/* $Id$ */

#include "qbuf.h"

void qbuf_float_split(struct qbuf* sq, struct qbuf* dq1, struct qbuf*dq2);

#endif

#include "v17dem.h"

#define INITIAL_BAUD_RATE 2400
#define INITIAL_CARRIER_FREQ 1800
#define SAMPLE_RATE 8000
#define SAMPLES_MAX (SAMPLES_RATE*5)           /* -> 5s buffer */

struct v17dem_state {
    int baud_rate;
    int carrier_freq;
    
    struct resample_state qam_resample, xam_resample, yam_resample;
    struct qbuf am_qbuf, xam_qbuf, yam_qbuf, xsymbols_qbuf, ysymbols_qbuf;
};


static int v17_7k2_symbol_decode(float x, float y) {
    int row, col, i;

    static const int v17_7k2_table[] = {
        3, 10,  5, 0,
        6, 15, 12, 9,
        13,  8, 11, 2,
        4,  1, 14, 7
    };

    col = (int) ((x+.6)/.4 +.5);
    if (col < 0) col = 0;
    if (col > 3) col = 3;

    row = (int) ((y+.6)/.4 +.5);
    if (row < 0) row = 0;
    if (row > 3) row = 3;

    i = col*4+row;

    assert(i >= 0 && i <= 15);

    return v17_7k2_table[i];
}

void v17dem_init(struct v17dem_state *s) {
    assert(s);

    memset(s, 0, sizeof(struct v17dem_state));
    s->baud_rate = INITIAL_BAUD_RATE;
    s->carrier_freq = INITIAL_CARRIER_FREQ;

    resample_init(&s->qam_resample);
    resample_init(&s->xam_resample);
    resample_init(&s->yam_resample);

    qbuf_init(&s->am_qbuf, sizeof(float) * SAMPLES_MAX);
    qbuf_init(&s->xam_qbuf, sizeof(float) * SAMPLES_MAX/2);
    qbuf_init(&s->yam_qbuf, sizeof(float) * SAMPLES_MAX/2);
    qbuf_init(&s->xsymbols_qbuf, sizeof(float) * SAMPLES_MAX/2);
    qbuf_init(&s->ysymbols_qbuf, sizeof(float) * SAMPLES_MAX/2);
}

void v17dem_done(struct v17dem_state *s) {
    assert(s);

    resample_done(&s->qam_resample);
    resample_done(&s->xam_resample);
    resample_done(&s->yam_resample);

    qbuf_done(&s->am_qbuf);
    qbuf_done(&s->xam_qbuf);
    qbuf_done(&s->yam_qbuf);
    qbuf_done(&s->xsymbols_qbuf);
    qbuf_done(&s->ysymbols_qbuf);
}

static int v17_decode(struct qbuf


int v17dem_run(struct v17dem_state *s, struct qbuf *sample_qbuf, struct qbuf *byte_qbuf){
    assert(s && sample_qbuf && byte_qbuf);

    resample_get(s->qam_resample, sample_qbuf, s->am_qbuf, SAMPLE_RATE, s->carrier_freq*4);
    qbuf_float_split(s->am_qbuf, s->xam_qbuf, s->yam_qbuf);
    resample_get(s->xam_resample, s->xam_qbuf, s->xsymbols_qbuf);
    resample_get(s->yam_resample, s->yam_qbuf, s->ysymbols_qbuf);

    v17_decode(s->xsymbols_qbuf, s->ysymbols_qbuf, byte_qbuf), 
    
}

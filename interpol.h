#ifndef foointerpolhfoo
#define foointerpolhfoo

struct interpol_state {
    int frac;
    float *coeff;
    int *coeff_valid;
    int radius;
};


void interpol_init(struct interpol_state *s, int frac, int radius);
void interpol_done(struct interpol_state *s);

float interpol_get(struct interpol_state *s, float *p, int l, float x);

#endif

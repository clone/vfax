#include <stdio.h>
#include <unistd.h>
#include <inttypes.h>
#include <assert.h>

#include "v17mod.h"
#include "v17tcm.h"
#include "util.h"

int16_t *conv_float_16sle(float *p, int l) {
    int i;
    int16_t *r = (int16_t*) p, *target = (int16_t*) p;

    assert(2*sizeof(int16_t) == sizeof(float));

    for (i = 0; i < l; i++) {
        int v = (int) (0x3FFF * *p);

        if (v < -0x8000) {
            v = -0x8000;
            fprintf(stderr, "UNDERFLOW: %f\n", *p);
        }

        if (v > 0x7fff) {
            v = 0x7fff;
            fprintf(stderr, "OVERFLOW\n");
        }
                        
        
        *(target++) = (int16_t) v;
        *(target++) = (int16_t) v;

        p++;
    }

    return r;
}

#ifndef TEST

int main() {
    struct v17mod_state mod;
    struct v17tcm_state tcm;

    v17mod_init(&mod);
    v17tcm_init(&tcm);

    for (;;) {
        ssize_t l;
        uint8_t buf[64];
        int i, n;
        float f[1024];

        //fprintf(stderr, "ITERATE\n");
        
        if ((l = read(0, buf, sizeof(buf))) <= 0) {
            //if (l < 0)
                fprintf(stderr, "read failure\n");

            
            goto finish;
        }

        for (i = 0; i < l; i++)
            buf[i] = v17tcm_encode(&tcm, buf[i] & 7);

        v17mod_push(&mod, buf, l);

        n = v17mod_pull(&mod, f, sizeof(f)/sizeof(float));

        
        
        if (loop_write(1, conv_float_16sle(f, n), n*sizeof(int16_t)*2) < 0) {
            fprintf(stderr, "write failure\n");
            goto finish;
        }
    }
    

finish:

    v17mod_done(&mod);
    
    return 0;
}

#endif

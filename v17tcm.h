#ifndef foov17tcmhfoo
#define foov17tcmhfoo

/* $Id$ */

#include <inttypes.h>

struct v17tcm_state {
    uint8_t y, c1, c2, c3;
};

void v17tcm_init(struct v17tcm_state *s);

uint8_t v17tcm_encode(struct v17tcm_state* s, uint8_t input);
uint8_t v17tcm_decode(struct v17tcm_state *s, uint8_t input);

#endif

#ifndef fooresamplehfoo
#define fooresamplehfoo

/* $Id$ */

#include "interpol.h"
#include "qbuf.h"

struct resample_state {
    float delta;

    int sfreq, dfreq;
    struct interpol_state interpol;
};

void resample_init(struct resample_state *s);
void resample_done(struct resample_state *s);
void resample_get(struct resample_state *s, struct qbuf *sq, struct qbuf *dq, int sfreq, int dfreq);

#endif

                 

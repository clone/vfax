#ifndef fooutilhfoo
#define fooutilhfoo

#include <sys/types.h>

ssize_t loop_write(int fd, const void *b, size_t l);

#endif

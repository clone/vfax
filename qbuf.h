#ifndef fooqbufhfoo
#define fooqbufhfoo

/* $Id$ */

#include <sys/types.h>

struct qbuf {
    void *data;
    size_t length, index, fill;
};

void qbuf_init(struct qbuf *q, size_t length);
void qbuf_done(struct qbuf *q);

void* qbuf_push(struct qbuf *q, size_t *l);
void qbuf_push_validate(struct qbuf *q, size_t l);

void* qbuf_pull(struct qbuf *q, size_t *l);
void qbuf_pull_invalidate(struct qbuf *q, size_t l);

#endif

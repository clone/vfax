#ifndef foobitsplithfoo
#define foobitsplithfoo

#include <inttypes.h>
#include <sys/types.h>

struct bitsplit_state {
    uint8_t in;
    int n_in;
    int maxbits_in;

    uint8_t out;
    int n_out;
    int maxbits_out;
};


void bitsplit_init(struct bitsplit_state *s, int maxbits_in, int maxbits_out);
void bitsplit(struct bitsplit_state *s, uint8_t *src, size_t *src_l, uint8_t *dst, size_t *dst_l);

#endif

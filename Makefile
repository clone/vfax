CFLAGS=-pipe -O0 -W -Wall -g -DTEST=7 -D_GNU_SOURCE
CC=gcc
LIBS=-lm

all: main

*.o: Makefile

main: main.o v17tcm.o v17mod.o bitsplit.o util.o interpol.o resample.o qbuf.o qbufsplit.o
	$(CC) -o $@ $^ $(LIBS)

clean: 
	rm -f main *.o

.PHONY: clean
